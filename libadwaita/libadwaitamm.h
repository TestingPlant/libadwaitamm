/* Copyright (c) 2019  Joe Hacker <joe@example.com>
 *
 * This file is part of libadwaitamm.
 *
 * libadwaitamm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * libadwaitamm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBADWAITAMM_H_INCLUDED
#define LIBADWAITAMM_H_INCLUDED

/** @mainpage libadwaitamm Reference Manual
 *
 * @section description Description
 *
 * The libadwaitamm C++ binding provides a C++ interface on top of the libadwaita
 * C library.
 *
 * @section overview Overview
 *
 * [...]
 *
 * @section use Use
 *
 * To use libadwaitamm in your C++ application, include the central header file
 * <tt>\<libadwaitamm.h\></tt>.  The libadwaitamm package ships a @c pkg-config
 * file with the correct include path and link command-line for the compiler.
 */

#include <libadwaitammconfig.h>
#include <libadwaitamm/preferences-row.h>
#include <libadwaitamm/toast-overlay.h>
#include <libadwaitamm/toast.h>

/** @example example/example.cc
 * A libadwaitamm example program.
 */

#endif /* !LIBADWAITAMM_H_INCLUDED */
