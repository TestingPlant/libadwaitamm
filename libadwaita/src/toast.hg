/* Copyright (c) 2019  Joe Hacker <joe@example.com>
 *
 * This file is part of libadwaitamm.
 *
 * libadwaitamm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * libadwaitamm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <adwaita.h>
#include <glibmm/object.h>

_DEFS(libadwaitamm,libadwaita)
_PINCLUDE(glibmm/private/object_p.h)

namespace Adw
{

_WRAP_ENUM(ToastPriority, AdwToastPriority)

class Toast : public Glib::Object
{
  _CLASS_GOBJECT(Toast, AdwToast, ADW_TOAST, Glib::Object, GObject)
  _STRUCT_NOT_HIDDEN

public:
  _WRAP_CTOR(Toast(const Glib::ustring& title), adw_toast_new)

  _WRAP_METHOD(void dismiss(), adw_toast_dismiss)
  _WRAP_METHOD(void set_detailed_action_name(const Glib::ustring& detailed_action_name), adw_toast_set_detailed_action_name)
  _WRAP_METHOD(void set_action_name(const Glib::ustring& action_name), adw_toast_set_action_name)
  _WRAP_METHOD(Glib::ustring get_action_name(), adw_toast_get_action_name)
  _WRAP_METHOD(void set_button_label(const Glib::ustring& button_label), adw_toast_set_button_label)
  _WRAP_METHOD(Glib::ustring get_button_label(), adw_toast_get_button_label)
  _WRAP_METHOD(void set_timeout(guint timeout), adw_toast_set_timeout)
  _WRAP_METHOD(guint get_timeout(), adw_toast_get_timeout)
  _WRAP_METHOD(void set_title(const Glib::ustring& title), adw_toast_set_title)
  _WRAP_METHOD(Glib::ustring get_title(), adw_toast_get_title)

  _WRAP_PROPERTY("action-name", Glib::ustring)
  _WRAP_PROPERTY("action-target", Glib::VariantBase)
  _WRAP_PROPERTY("button-label", Glib::ustring)
  _WRAP_PROPERTY("priority", ToastPriority)
  _WRAP_PROPERTY("timeout", guint)
  _WRAP_PROPERTY("title", Glib::ustring)
};

} // namespace Adw
