/* Copyright (c) 2019  Joe Hacker <joe@example.com>
 *
 * This file is part of libadwaitamm.
 *
 * libadwaitamm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * libadwaitamm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glibmm_generate_extra_defs/generate_extra_defs.h>
#include <adwaita.h>
#include <iostream>

int main(int, char**)
{
  // libadwaita_init();

  std::cout << get_defs( ADW_TYPE_ACTION_ROW )
            << get_defs( ADW_TYPE_ANIMATION )
            << get_defs( ADW_TYPE_ANIMATION_STATE )
            << get_defs( ADW_TYPE_ANIMATION_TARGET )
            << get_defs( ADW_TYPE_APPLICATION )
            << get_defs( ADW_TYPE_APPLICATION_WINDOW )
            << get_defs( ADW_TYPE_AVATAR )
            << get_defs( ADW_TYPE_BIN )
            << get_defs( ADW_TYPE_BUTTON_CONTENT )
            << get_defs( ADW_TYPE_CALLBACK_ANIMATION_TARGET )
            << get_defs( ADW_TYPE_CAROUSEL )
            << get_defs( ADW_TYPE_CAROUSEL_INDICATOR_DOTS )
            << get_defs( ADW_TYPE_CAROUSEL_INDICATOR_LINES )
            << get_defs( ADW_TYPE_CENTERING_POLICY )
            << get_defs( ADW_TYPE_CLAMP )
            << get_defs( ADW_TYPE_CLAMP_LAYOUT )
            << get_defs( ADW_TYPE_CLAMP_SCROLLABLE )
            << get_defs( ADW_TYPE_COLOR_SCHEME )
            << get_defs( ADW_TYPE_COMBO_ROW )
            << get_defs( ADW_TYPE_EASING )
            << get_defs( ADW_TYPE_ENUM_LIST_ITEM )
            << get_defs( ADW_TYPE_ENUM_LIST_MODEL )
            << get_defs( ADW_TYPE_EXPANDER_ROW )
            << get_defs( ADW_TYPE_FLAP )
            << get_defs( ADW_TYPE_FLAP_FOLD_POLICY )
            << get_defs( ADW_TYPE_FLAP_TRANSITION_TYPE )
            << get_defs( ADW_TYPE_FOLD_THRESHOLD_POLICY )
            << get_defs( ADW_TYPE_HEADER_BAR )
            << get_defs( ADW_TYPE_LEAFLET )
            << get_defs( ADW_TYPE_LEAFLET_PAGE )
            << get_defs( ADW_TYPE_LEAFLET_TRANSITION_TYPE )
            << get_defs( ADW_TYPE_NAVIGATION_DIRECTION )
            << get_defs( ADW_TYPE_PREFERENCES_GROUP )
            << get_defs( ADW_TYPE_PREFERENCES_PAGE )
            << get_defs( ADW_TYPE_PREFERENCES_ROW )
            << get_defs( ADW_TYPE_PREFERENCES_WINDOW )
            << get_defs( ADW_TYPE_SPLIT_BUTTON )
            << get_defs( ADW_TYPE_SPRING_ANIMATION )
            << get_defs( ADW_TYPE_SPRING_PARAMS )
            << get_defs( ADW_TYPE_SQUEEZER )
            << get_defs( ADW_TYPE_SQUEEZER_PAGE )
            << get_defs( ADW_TYPE_SQUEEZER_TRANSITION_TYPE )
            << get_defs( ADW_TYPE_STATUS_PAGE )
            << get_defs( ADW_TYPE_STYLE_MANAGER )
            << get_defs( ADW_TYPE_SWIPEABLE )
            << get_defs( ADW_TYPE_SWIPE_TRACKER )
            << get_defs( ADW_TYPE_TAB_BAR )
            << get_defs( ADW_TYPE_TAB_PAGE )
            << get_defs( ADW_TYPE_TAB_VIEW )
            << get_defs( ADW_TYPE_TIMED_ANIMATION )
            << get_defs( ADW_TYPE_TOAST )
            << get_defs( ADW_TYPE_TOAST_OVERLAY )
            << get_defs( ADW_TYPE_TOAST_PRIORITY )
            << get_defs( ADW_TYPE_VIEW_STACK )
            << get_defs( ADW_TYPE_VIEW_STACK_PAGE )
            << get_defs( ADW_TYPE_VIEW_SWITCHER )
            << get_defs( ADW_TYPE_VIEW_SWITCHER_BAR )
            << get_defs( ADW_TYPE_VIEW_SWITCHER_POLICY )
            << get_defs( ADW_TYPE_VIEW_SWITCHER_TITLE )
            << get_defs( ADW_TYPE_WINDOW )
            << get_defs( ADW_TYPE_WINDOW_TITLE )
	    ;
  return 0;
}
