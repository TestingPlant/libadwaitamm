dnl Copyright (c) 2019  Joe Hacker <joe@example.com>
dnl This file is part of libadwaitamm.

_CONVERSION(`AdwSomeType*',`Glib::RefPtr<SomeType>',`Glib::wrap($3)')
